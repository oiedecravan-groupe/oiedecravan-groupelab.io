import React, { useReducer, useState } from 'react';
import { GatsbyImage } from "gatsby-plugin-image";
import Select from 'react-select';
import { FaExternalLinkAlt } from 'react-icons/fa';
import { useDestination } from '../components/destination';
import { Link } from 'gatsby';
import { useTheme } from '@emotion/react';
import { useLang } from '../components/lang';
import page from '../components/layout';
import { useClickOutside } from '../components/clickOutside';
import * as Icons from '../components/icons';
import {
  Card,
  Clickable,
  Video,
  Disk,
  H2Icon,
  H2,
  H3,
  Box,
  Body1,
  Flex,
  GridMd,
  Tilde,
  Subtitle,
  TextCard,
} from '../components/elements';
import nuage from '../icons/nuage.svg';
import { ArrowLeft, ArrowRight } from '../components/icons';
import { cleanBook, unP } from '../utils';
import { typo_ajust } from '../../util';
import {  withPrefix } from 'gatsby';

function Nuage() {
  return (
    <Box px='10px' pb='2px'>
      <img src={nuage} alt='nuage' height='8px' />
    </Box>
  );
}

function BackLink() {
  const theme = useTheme();
  const { lang, textes } = useLang();
  return (
    <Link to={lang === 'fr' ? '/catalogue' : '/en/catalogue'}>
      <Flex justifyContent='space-between' alignItems='flex-end'>
        <Box pr='8px' pb='4px'>
          <ArrowLeft />
        </Box>
        <Box
          t='20px'
          {...theme.styles.buttonSmall}
          dangerouslySetInnerHTML={{ __html: textes['retour'] }}
        />
        <Box flexGrow='1' />
      </Flex>
    </Link>
  );
}

function getCartHref({ lang, href, book, conversions, destination }) {
  const conversion = conversions.find(
    ({ Destination: destination_ }) => destination === destination_,
  );
  if (!conversion) {
    console.error(`Unkown destination ${destination}`);
    return;
  }
  const amount = Math.round(
    (book.prixCAD + conversion['Frais_de_port__CAD_']) *
      conversion['Taux_de_conversion'],
  );
  const query = {
    add: 1,
    cmd: '_cart',
    business: 'lentement@oiedecravan.com',
    item_name: `${book.auteurLivre} - ${book.titre}`,
    amount,
    currency_code: conversion['Devise'],
    lc: lang,
    bn: 'PP-ShopCartBF',
    charset: 'iso-8859-15',
    return: href,
    shopping_url: withPrefix('catalogue'),
    no_shipping: 0,
    no_note: 0,
  };
  return 'https://www.paypal.com/cgi-bin/webscr?' + new URLSearchParams(query);
}

function SelectDestination({ conversions, handler }) {
  const ref = useClickOutside(() => {
    handler();
  });
  const { textes } = useLang();
  const options = conversions.map(({ Destination: destination }) => ({
    value: destination,
    label: destination,
  }));
  const [selectedOption, setSelectedOption] = useState(null);
  const clickHandler = () => {
    handler(selectedOption?.value);
  };
  return (
    <Flex
      flexDirection='column'
      css={{
        position: 'fixed',
        left: '0px',
        top: '10px',
        zIndex: 10,
      }}
      height='100%'
      width='100%'
    >
      <Flex justifyContent='center' height='100%' width='100%'>
        <Flex
          justifyContent='center'
          alignItems='center'
          color='accent'
          flexDirection='column'
        >
          <Box ref={ref} p='4px' backgroundColor='background'>
            <Card>
              <Flex flexDirection='column' padding='10px' css={{ gap: '10px' }}>
                <Box
                  color='primary'
                  dangerouslySetInnerHTML={{ __html: textes['lieu'] }}
                />
                <Box color='background' textAlign='left'>
                  <Select
                    placeholder={'...'}
                    defaultValue={selectedOption}
                    onChange={setSelectedOption}
                    options={options}
                  />
                </Box>
                <Box
                  color='primary'
                  dangerouslySetInnerHTML={{
                    __html: textes['notice'],
                  }}
                ></Box>
                <Clickable onClick={clickHandler}>
                  <Box
                    color='accent'
                    dangerouslySetInnerHTML={{
                      __html: textes['continuer'],
                    }}
                  />
                </Clickable>
              </Flex>
            </Card>
          </Box>
        </Flex>
      </Flex>
    </Flex>
  );
}

function BouttonDAchat({ handler }) {
  const theme = useTheme();
  const { textes } = useLang();
  return (
    <Clickable onClick={handler}>
      <Flex color='accent' {...theme.styles.button}>
        <Box
          p='10px'
          borderWidth='1px'
          borderStyle='solid'
          dangerouslySetInnerHTML={{ __html: unP(textes['Disponible']) }}
        />
      </Flex>
    </Clickable>
  );
}

function Epuise({ disponibilite }) {
  const theme = useTheme();
  const { textes } = useLang();
  return (
    <Flex color='muted' {...theme.styles.button}>
      <Box
        p='10px'
        borderWidth='1px'
        borderStyle='solid'
        dangerouslySetInnerHTML={{
          __html: unP(textes[disponibilite] ?? '<p></p>'),
        }}
      />
    </Flex>
  );
}

function Share() {
  const theme = useTheme();
  return (
    <Flex color='accent' {...theme.styles.button}>
      <Box>Partager </Box>
    </Flex>
  );
}

function useCycle(length) {
  const reducer = (state, action) => {
    let position;
    switch (action.type) {
      case 'CYCLE':
        position = state.position;
        position++;
        if (position === state.length) position = 0;
        return { ...state, position };
      case 'BACKCYCLE':
        position = state.position;
        if (position === 0) position = state.length;
        position--;
        return { ...state, position };
      case 'GO':
        return { ...state, position: action.position };
      default:
        throw Error('Unkown action');
    }
  };

  const [state, dispatch] = useReducer(reducer, { position: 0, length });
  const cycle = () => dispatch({ type: 'CYCLE' });
  const backcycle = () => dispatch({ type: 'BACKCYCLE' });
  const go = (position) => dispatch({ type: 'GO', position });
  return {
    position: state.position,
    cycle,
    backcycle,
    go,
  };
}

function BookCol({ book, ...props }) {
  const { lang, textes } = useLang();
  const theme = useTheme();
  const { position, cycle, go } = useCycle(book.couvertures.length);
  return (
    <Box
      color='accent'
      {...theme.styles.button}
      textTransform='uppercase'
      {...props}
      pb='15px'
    >
      {book.couvertures.length > 0 && (
        <>
          <Clickable onClick={cycle}>
            <Flex flexDirection='colum' justifyContent='center'>
              <Box maxWidth='500px'>
                <GatsbyImage image={book.couvertures[position]} alt='couverture' />
              </Box>
            </Flex>
          </Clickable>
          <Box pb='15px' />
        </>
      )}
      {book.couvertures.length > 1 && (
        <Flex justifyContent='center'>
          {book.couvertures.map((_, index) => (
            <Disk
              key={index}
              active={index === position}
              onClick={() => go(index)}
            />
          ))}
        </Flex>
      )}
      <Box pb='20px' />
      <Box py={['20px', '0px']}>
        {book.ISBN && <Box>ISBN: {book.ISBN}</Box>}
        {book.annee && (
          <Link to={`${lang === 'fr' ? '/' : '/en/'}catalogue?q=${book.annee}`}>
            {book.annee}
          </Link>
        )}
        {book.hauteur && book.largeur && (
          <Box>
            {book.hauteur} x {book.largeur} cm
          </Box>
        )}
        {book.pages && (
          <Box
            dangerouslySetInnerHTML={{
              __html: book.pages + '&nbsp;' + unP(textes['pages']),
            }}
          />
        )}
      </Box>
    </Box>
  );
}

function SignatureText(data) {
  const signature = typo_ajust(data.Texte__signature)?.trim();
  const description_italiques = typo_ajust(
    data['Texte__description_italiques']?.trim(),
  );
  const description_romain = typo_ajust(
    data['Texte__description_romain'],
  )?.trim();
  return (
    <>
      {signature}
      {signature && (description_romain || description_italiques) && ', '}
      <i>{description_italiques}</i>
      {description_italiques && description_romain && ' – '}
      {description_romain}
    </>
  );
}

function SignatureLink({ children, ...data }) {
  const href = data['Texte__hyperlien'];
  if (href) {
    return (
      <Link href={href}>
        {children}
        <FaExternalLinkAlt css={{ marginLeft: 10 }} size={14} />
      </Link>
    );
  }
  return children;
}

function EmbbededText(data) {
  const texte = data.Texte_contenu?.childMarkdownRemark.html;
  const theme = useTheme();
  if (!texte) return null;
  return (
    <TextCard>
      <Box color='accent'>
        <Box
          {...theme.styles.quoteSmall}
          dangerouslySetInnerHTML={{ __html: texte }}
        />
        <Box pt='20px' {...theme.styles.subtitle}>
          <SignatureLink {...data}>
            <SignatureText {...data} />
          </SignatureLink>
        </Box>
      </Box>
    </TextCard>
  );
}

function EmbbededYoutube(data) {
  const id = data['Youtube__URL']?.match(re)?.[1];
  // const caption = data['Image__l_gende'];
  // const theme = useTheme();
  if (!id) return null;
  return (
    <>
      <Box width='100%' height='500px'>
        <Video url={'https://www.youtube.com/embed/' + id} title={'video'} />
      </Box>
      {/* {caption && <Box pt='20px' {...theme.styles.body2}> */}
      {/*   <i>{caption}</i> */}
      {/* </Box>} */}
    </>
  );
}

const re = /youtube.com\/watch\?v=(.+)/;

function AutourDuLivre0({ autour }) {
  const { position, cycle, backcycle } = useCycle(autour.length);
  const { textes } = useLang();
  return (
    <>
      <H2Icon
        pt={['60px', '160px']}
        Icon={Icons.Ecrire}
        dangerouslySetInnerHTML={{ __html: unP(textes['h2 0']) }}
      />
      <Box pb={['40px', '60px']} />
      <GridMd>
        {autour.length > 1 && (
          <Box gcs={3} gce={4} justifyContent='end' alignSelf='center'>
            <Clickable onClick={backcycle}>
              <Box py='10px'>
                <ArrowLeft />
              </Box>
            </Clickable>
          </Box>
        )}
        <Box gcs={4} gce={10}>
          <EmbbededYoutube {...autour[position]} />
          <EmbbededText {...autour[position]} />
        </Box>
        {autour.length > 1 && (
          <Box gcs={11} gce={12} alignSelf='center'>
            <Clickable onClick={cycle}>
              <Box py='10px'>
                <ArrowRight />
              </Box>
            </Clickable>
          </Box>
        )}
      </GridMd>
      <Box pb={['40px', '60px']} />
    </>
  );
}

function AutourDuLivre({ autour }) {
  if (autour.length === 0) return null;
  return <AutourDuLivre0 autour={autour} />;
}

function Main({ location: { href }, data }) {
  const { lang } = useLang();
  const book = cleanBook(lang, data.airtableCatalogue);
  const autour = data.allAirtableAutourDuLivre.nodes.map(({ data }) => data);
  const conversions = data.allAirtableConversions.nodes.map(({ data }) => data);
  const [destination, setDestination] = useDestination();
  const buy = (destination_) => {
    const cartHref = getCartHref({
      lang,
      href,
      book,
      conversions,
      destination: destination_,
    });
    window.location.href = cartHref;
  };
  const selectDestinationHandler = (destination_) => {
    setDestination(destination_);
    setAskingDestination(false);
    if (destination_) buy(destination_);
  };
  const buyHandler = () => {
    if (destination) buy(destination);
    else setAskingDestination(true);
  };
  const [askingDestination, setAskingDestination] = useState(false);
  return (
    <>
      {askingDestination && (
        <SelectDestination
          conversions={conversions}
          handler={selectDestinationHandler}
        />
      )}
      <Box pb={['40px', '60px']} />
      <GridMd>
        <Box gcs={2} gce={12} display={['none', 'none', 'inherit']}>
          <BackLink />
          <Box pb={['40px', '60px']} />
        </Box>
        <Box gcs={2} gce={5} display={['none', 'none', 'inherit']}>
          <BookCol book={book} />
        </Box>
        <Box gcs={[0, 2, 6]} gce={[13, 12, 12]}>
          <H2 color='accent'>{typo_ajust(book.titre)}</H2>
          <Box color='accent'>
            <Tilde />
          </Box>
          <Link
            to={`${lang === 'fr' ? '/' : '/en/'}catalogue?q=${encodeURI(
              book.auteur,
            )}`}
          >
            <H3 color='accent' textTransform='uppercase'>
              {typo_ajust(book.auteurLivre)}
            </H3>
          </Link>
          {book.createursSecondaires && (
            <Subtitle>{typo_ajust(book.createursSecondaires)}</Subtitle>
          )}
          <Box pb='40px' />
          <Flex color='accent' alignItems='baseline'>
            {book.genre && (
              <Link
                to={`${lang === 'fr' ? '/' : '/en/'}catalogue?q=${book.genre}`}
              >
                <Subtitle>{typo_ajust(book.genre)}</Subtitle>
              </Link>
            )}
            {book.genre && book.collection && (
              <Box px='10px'>
                <Nuage />
              </Box>
            )}
            {book.collection && (
              <Link
                to={`${lang === 'fr' ? '/' : '/en/'}catalogue?q=${
                  book.collection
                }`}
              >
                <Subtitle>{book.collection}</Subtitle>
              </Link>
            )}
          </Flex>
          <Box pb='40px' />
          {!process.env.GATSBY_NO_TRANSACTION &&
            (book.disponibilite === 'Disponible' ? (
              <BouttonDAchat handler={buyHandler} />
            ) : (
              <Epuise disponibilite={book.disponibilite} />
            ))}
          <Box pb='40px' />
          <Box display={['inherit', 'inherit', 'none']}>
            <BookCol book={book} />
          </Box>
          <Body1>
            <div
              dangerouslySetInnerHTML={{
                __html:
                  data.airtableCatalogue.data['Presentation_et_bio_' + lang]
                    ?.childMarkdownRemark.html,
              }}
            />
          </Body1>
          {/*<Share />*/}
        </Box>
      </GridMd>
      <AutourDuLivre autour={autour} />
    </>
  );
}

export default page(Main);
